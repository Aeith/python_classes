import math


class Poly:

    def __init__(self, *args, **kwargs):
        poly = []
        for x in args:
            poly.append(x)

        self.coeff = poly

    def __str__(self):
        poly = []
        for x in range(len(self.coeff)):
            if self.coeff[x] < 0:
                operator = "-"
            else:
                operator = "+"

            if x == 0:
                poly.append(str(self.coeff[x]))
            elif x == 1:
                poly.append(operator + str((self.coeff[x]))+"x")
            else:
                poly.append(str((self.coeff[x]))+"x^"+str(x))

        return poly

    def evalue(self, x=0):
        operators = []
        polynomials = ["" for x in range(len(self.coeff))]
        for count, poly in enumerate(self.__str__()):
            # Second element and so on...
            if "x" in poly:
                operators.append(poly[0])

                # Power
                if "^" in poly:
                    power_index = poly.index("^")+1
                    number = x
                    power = int(poly[power_index:])
                    x = math.pow(number, power)

                polynomials[count] = int(poly[1]) * x
            else:
                polynomials[count] = int(poly)

        total = 0
        for e in range(len(polynomials)):
            if e > 0:
                if operators[e-1] == "+":
                    total += polynomials[e]
                else:
                    total -= polynomials[e]
            else:
                total += polynomials[e]

        print(total)


p = Poly(3, 4, -2)
print(p.coeff)
print(p.__str__())
p.evalue(1)
p.evalue(2)
