class Domino:

    def __init__(self, face_a = 1, face_b = 1):
        self.face_a = face_a
        self.face_b = face_b

    def __repr__(self):
        return {"Face A": self.face_a, "Face B": self.face_b}

    def affiche_points(self):
        print("Face A " + str(self.face_a) + " Face B " + str(self.face_b))

    def totale(self):
        return self.face_a + self.face_b


d = Domino(4, 6)
d.affiche_points()
x = d.totale()
print(x)