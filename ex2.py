class CompteBancaire:

    def __init__(self, nom, solde=0):
        self.nom = nom
        self.solde = solde

    def depot(self, somme=0):
        if somme > 0:
            self.solde += somme
        else:
            self.retrait(somme)

    def retrait(self, somme=0):
        if somme > 0:
            self.solde -= somme
        else:
            self.depot(somme)

    def __repr__(self):
        return {"Propriétaire": self.nom, "Solde": self.number}

    def affiche(self):
        print("Bonjour " + self.nom + " ! Votre solde courant est de : " + str(self.solde))


compte1 = CompteBancaire("Jean", 1000)
compte1.retrait(200)
compte1.affiche()
compte2 = CompteBancaire("Marc")
compte2.depot(500)
compte2.affiche()