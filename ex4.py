import math


class Fraction:

    def __init__(self, num=0, denom=1):
        """
        Constructor
        :param num: numerator
        :param denom: denominator
        """
        self.num = num
        if denom != 0:
            self.denom = denom
        else:
            print("Dénumérateur égal à 0 ; valeur par défaut définie à 1")
            self.denom = 1

    def __add__(self, other):
        """
        Override add method
        :param other: other object
        :return: new object
        """
        # Same denom
        if self.denom != other.denom:
            current_denom = self.denom
            self.num *= other.denom
            self.denom *= other.denom
            other.num *= current_denom
            other.denom *= current_denom

        num = self.num+other.num
        denum = self.denom
        gcd = math.gcd(num, denum)

        return Fraction(int(num/gcd), int(denum/gcd))

    def __sub__(self, other):
        """
        Override sub method
        :param other: other object
        :return: new object
        """
        # Same denom
        if self.denom != other.denom:
            current_denom = self.denom
            self.num *= other.denom
            self.denom *= other.denom
            other.num *= current_denom
            other.denom *= current_denom

        num = self.num-other.num
        denum = self.denom
        gcd = math.gcd(num, denum)

        return Fraction(int(num/gcd), int(denum/gcd))

    def __mul__(self, other):
        """
        Override mul method
        :param other: other object
        :return: new object
        """

        num = self.num*other.num
        denum = self.denom*other.denom
        gcd = math.gcd(num, denum)

        return Fraction(int(num/gcd), int(denum/gcd))

    def __truediv__(self, other):
        """
        Override div method
        :param other: other object
        :return: new object
        """

        num = self.num*(1/other.num)
        denum = self.denom*(1/other.denom)

        # Kinda messy though
        while str(denum)[-2:] != ".0" or str(num)[-2:] != ".0":
            num *= 2
            denum *= 2

        gcd = math.gcd(int(num), int(denum))

        return Fraction(int(num/gcd), int(denum/gcd))

    def __repr__(self):
        return {"Numérateur": self.num, "Dénominateur":self.denom}

    def affiche(self):
        """
        Display the fraction
        :return: void
        """
        print(str(self.num) + "/" + str(self.denom))


f = Fraction(3, 4)
f.affiche()
g = Fraction(1, 2)
g.affiche()
r1 = f + g
r1.affiche()
r2 = f / g
r2.affiche()