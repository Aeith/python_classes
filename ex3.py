def generator():
    x = 0
    while True:
        yield x
        x += 1


class TableMultiplication:

    def __init__(self, number=0):
        self.number = number
        self.gen = generator()

    def __repr__(self):
        return {"Nombre": self.number}

    def prochain(self):
        print(self.gen.__next__()*self.number)


table_de_7 = TableMultiplication(7)
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
table_de_7.prochain()
